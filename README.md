# React Native Evaluation

## Setup Your Enivonrment

Please clone this repository locally. You should have npm and react native setup locally.

## Instructions 

Few simple modification to an existing react-native application.

Add a single button to the main view. The button text should read ‘Capture’.

When the button is clicked, trigger an action that grabs the current timestamp and sends it to a Record reducer (please create). The reducer should store/save all in hash named timestamps. Add a debug message to display all Record.timestamps on each click.

Please also install and use a linter.

## Delivery

Contact office+evaluations@habitnature.com to get your account added to this repository.

Submit a pull request to this repository with your changes.


